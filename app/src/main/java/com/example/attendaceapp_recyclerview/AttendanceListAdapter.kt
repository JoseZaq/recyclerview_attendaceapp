package com.example.attendaceapp_recyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textview.MaterialTextView

class AttendanceListAdapter(val attendances:List<Attendance>):RecyclerView.Adapter<AttendanceListAdapter.AttendanceListAdapterViewHolder>() {

    // provee referencias de las vistas dentro de cada item
    class AttendanceListAdapterViewHolder(view: View):RecyclerView.ViewHolder(view){
        val tvName:MaterialTextView
        val tvModule:MaterialTextView
        val tvDate:MaterialTextView
        val imgJustify:ImageView
        init {
            tvName=view.findViewById(R.id.tv_name)
            tvModule=view.findViewById(R.id.tv_module)
            tvDate=view.findViewById(R.id.tv_date)
            imgJustify=view.findViewById(R.id.img_justified)
        }
    }

    // crea nuevas views, llamado por el layoutManager
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AttendanceListAdapterViewHolder {
        // creo una nueva vista, que lo define el UI de la lista
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.attendance_item,parent,false)
        return AttendanceListAdapterViewHolder(view)
    }

    // reemplaza el contenido de un vista, llamado por el Layoutmanager
    override fun onBindViewHolder(holder: AttendanceListAdapterViewHolder, position: Int) {
        holder.tvName.text = attendances[position].name
        holder.tvModule.text = attendances[position].module
        holder.tvDate.text = attendances[position].date
        if( attendances[position].justified)
            holder.imgJustify.setImageResource(R.drawable.ic_baseline_done_outline_24)
        else
            holder.imgJustify.setImageResource(R.drawable.ic_baseline_warning_24)
    }

    override fun getItemCount(): Int {
        return attendances.size
    }
}