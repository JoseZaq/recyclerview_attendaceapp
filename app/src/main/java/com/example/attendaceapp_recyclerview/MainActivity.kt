package com.example.attendaceapp_recyclerview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.commit
import androidx.fragment.app.replace

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // añadir fragment
        supportFragmentManager.commit {
            replace<AbsenceFragment>(R.id.nav_host_fragment).setReorderingAllowed(true)
                .addToBackStack(null)
        }

    }
}