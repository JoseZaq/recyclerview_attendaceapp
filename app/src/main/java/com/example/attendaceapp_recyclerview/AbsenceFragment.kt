package com.example.attendaceapp_recyclerview

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.attendaceapp_recyclerview.viewModel.AbsenceViewModel
import com.google.android.material.button.MaterialButton
import com.google.android.material.checkbox.MaterialCheckBox
import com.google.android.material.textfield.TextInputEditText
import java.text.DateFormat
import java.time.LocalDate
import java.time.LocalTime
import java.util.*

class AbsenceFragment: Fragment(R.layout.absence) {
    // vars
    lateinit var txName:TextInputEditText
    lateinit var spModules:Spinner
    lateinit var btDate:MaterialButton
    lateinit var chkJustified:MaterialCheckBox
    lateinit var btAdd:MaterialButton
    // viewModel
    private val absenceViewModel:AbsenceViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // views
        txName = view.findViewById(R.id.td_name)
        spModules = view.findViewById(R.id.sp_modules)
        btDate = view.findViewById(R.id.bt_date)
        chkJustified = view.findViewById(R.id.chk_justified)
        btAdd = view.findViewById(R.id.bt_add)
        ///
        btDate.text = Date().toString()
        // listeners
        btAdd.setOnClickListener{
            // añade una nueva asistencia en el viewModel
            val att = Attendance(txName.text.toString(),spModules.selectedItem.toString(),
                btDate.text.toString(),chkJustified.isChecked)
            absenceViewModel.addAttendace(att)
            // muestra un alterDialog
            val alertDialog:AlertDialog? = activity?.let{
                val builder = AlertDialog.Builder(it)
                builder.apply {
                    setPositiveButton("Ok",
                    DialogInterface.OnClickListener{dialog,id -> txName.setText("") })
                }
                builder.setTitle("Missed Attendance Created")
                builder.setMessage("The Student ${att.name} has missed ${att.module}" +
                        "on ${att.date} with${if(att.justified) " justification" else "out justification"}")
                builder.create()
            }
            alertDialog?.show()
        }
    }
}