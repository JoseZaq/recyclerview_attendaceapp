package com.example.attendaceapp_recyclerview.viewModel

import android.util.Log
import android.util.Log.WARN
import androidx.lifecycle.ViewModel
import com.example.attendaceapp_recyclerview.Attendance
import com.example.attendaceapp_recyclerview.R
import java.sql.Date
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.random.Random

class AbsenceViewModel: ViewModel()  {
    // vars
    var attendanceList = mutableListOf<Attendance>()
    // contructor
    init{
        val modules=listOf<String>("M9-Programacion de Servicios y Procesos",
        "M17-Desarrollo de Aplicaciones e Interficies",
        "M8-Programacion de Multimedia y Dispositivos",
        "M7-Desarrollo de Interfaces",
        "M6-Acceso de Datos",
        "M13-Proyecto")
        val df: DateFormat = SimpleDateFormat("MMM d, yyyy")
        val now: String = df.format(Date())
        for (i in 1..100){
            attendanceList.add(
                Attendance("Student #$i", modules[(5*Math.random()).toInt()],now,i % 3 == 0)
            )
        }
    }
    // functions
    fun addAttendace(attendance: Attendance){
        attendanceList.add(attendance)
    }
    fun getAttendances(): MutableList<Attendance> {
        return attendanceList
    }

}