package com.example.attendaceapp_recyclerview

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.attendaceapp_recyclerview.viewModel.AbsenceViewModel

class AttendanceListFragment:Fragment(R.layout.list_fragment) {
    // views
    lateinit var recyclerView:RecyclerView
//     var clickListener:View.OnClickListener = () -> findNavController().navigate(R.id.action_attendanceListFragment_to_absenceFragment)
    val viewModel: AbsenceViewModel by viewModels()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // views
        recyclerView = view.findViewById(R.id.recyclerView_container)
        // RECYCLER VIEW
        /// asignamos un adapter para el recyclerView
        recyclerView.adapter = AttendanceListAdapter(viewModel.getAttendances())
//        recyclerView.addOnItemTouchListener()
    }
}