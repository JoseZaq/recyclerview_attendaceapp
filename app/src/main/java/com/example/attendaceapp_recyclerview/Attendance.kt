package com.example.attendaceapp_recyclerview

import java.util.*

data class Attendance(var name:String,var module:String,var date:String,var justified:Boolean )
